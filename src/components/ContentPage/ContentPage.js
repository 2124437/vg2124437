/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React, { PropTypes, Component } from 'react';
import styles from './ContentPage.css';
import withStyles from '../../decorators/withStyles'; 

@withStyles(styles)
class ContentPage extends Component {

  static propTypes = {
    path: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    title: PropTypes.string,
  };

  static contextTypes = {
    onSetTitle: PropTypes.func.isRequired,
  }; 
  constructor(props) { 
    super(props);
    this.state = {hosts: []};
  }


  render() {
    this.context.onSetTitle(this.props.title); 
      if(typeof window !== 'object') { // we are on the server, cross domain is fine!
            var getArrayOfMostFrequentProperty = function (arrayOfObjects, property) {
              return u.chain(arrayOfObjects)
                                .countBy(property)
                                .pairs()
                                .sortBy(1).reverse()
                                .pluck(0)
                                .value()
                                .splice(0, 5); 
            },
                getLisHTML = function(property) { //  cannot do pretty react rendering because React.setState can't be run on the server :(
                  var lis = "";
                 u.each(getArrayOfMostFrequentProperty(logEntriesObjects, property), function(value) {
                   lis += '<li>' + value +'</li>';
                 });
                  return {__html: lis};
               }
            var request = require('sync-request'),
                URL = require('url-parse'),
                u = require('underscore'),
                logEntriesObjects = [],
                scope = this,
                res = request('GET', 'http://tech.vg.no/intervjuoppgave/varnish.log'), // should be cached in prod
                body = res.getBody(),
                logEntries = body.toString().split('\n');  
            u.each(logEntries, function(logEntry) {
              if(logEntry.length > 1) { 
                var logEntryArray = logEntry.split(' '),
                    url = new URL(logEntryArray[6].toString()),
                    urlHost = url.host;         
                logEntriesObjects.push({
                  rawArray: logEntryArray,
                  host: urlHost,
                  url: url
                });
              } 
           });   
      } else { // run on the client

                var getLisHTML = function(property) { return false; }
      }
    return (
      <div className="ContentPage">
        <div className="ContentPage-container"> 
          <h3>Top hosts</h3>
          <ul dangerouslySetInnerHTML={getLisHTML('host')} /> 
          <h3>Top fetched content</h3>
          <ul dangerouslySetInnerHTML={getLisHTML('url')} />
        </div>
      </div>
    );
  }

} 

export default ContentPage;
